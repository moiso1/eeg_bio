package cr.ac.tec.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.InputStream;

import org.junit.Test;

import cr.ac.tec.model.Snippets;

public class BindingTest {

	
	@Test
	public void testbinding(){
		try{
			InputStream is = BindingTest.class.getClassLoader().getResourceAsStream("salida1.xml");
			Snippets snippets = JdomLoader.parseDocument(is);
			assertEquals(expectedSnippets, snippets.toString().replaceAll("\\s", ""));
		}
		catch(Exception e){
			e.printStackTrace();
			fail("Some exception");
		}
	}
	
	private String expectedSnippets = "Snippets[[Snippet[Category[name=distribucion],Taxon[family=Haloragaceae,genre=,species=],"
			+ "Text=9gén.yaproximadamente145especies,Alaska,CanadáyGroenlandia-SChileyVenezuela,Brazil,Paraguay,Uruguay,Argentina,"
			+ "IslasMalvinas,AntillasMayores,Bahamas,ViejoMundo;1gén.y1especieenCostaRica.],Snippet[Category[name=distribucion],"
			+ "Taxon[family=Haloragaceae,genre=Myriophyllum,species=],Text=Aproximadamente60especies,Alaska,CanadáyGroenlandia-SChi"
			+ "leyVenezuela,Brazil,Paraguay,Uruguay,Argentina,IslasMalvinas,AntillasMayores,Eurasia,Australia.]]]";
}
