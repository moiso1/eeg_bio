package cr.ac.tec.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.InputStream;
import java.util.Map;

import org.junit.Test;

import cr.ac.tec.model.Distribucion;
import cr.ac.tec.model.Snippet;
import cr.ac.tec.model.Snippets;
import cr.ac.tec.parser.freeling.Parser;

public class ParseTest {
	@Test
	public void testbinding(){
		try{
			System.out.println("#### Salida1.xml ####");
			InputStream is = BindingTest.class.getClassLoader().getResourceAsStream("salida1.xml");
			Snippets snippets = JdomLoader.parseDocument(is);
//			assertEquals(expectedSnippets, snippets.toString().replaceAll("\\s", ""));
			Parser parser = new Parser();
			parser.init();
			Snippet firstSnip = snippets.getSnippet().get(2);
			parser.parseSnippet(firstSnip);
			assertEquals("(Hammel & Pérez 24277;Costa Rica, INB, MO)".replaceAll("\\s", ""), firstSnip.getEspecimen().trim().replaceAll("\\s", ""));
			//0-2200+ metros
			assertEquals("0-2200+metros", firstSnip.getElevacion().trim());
			assertEquals(1, firstSnip.getFloracion().size());
		}
		catch(Exception e){
			e.printStackTrace();
			fail("Some exception");
		}
	}
	
	@Test
	public void testbinding2(){
		try{
			System.out.println("#### Salida2.xml ####");
			InputStream is = BindingTest.class.getClassLoader().getResourceAsStream("salida2.xml");
			Snippets snippets = JdomLoader.parseDocument(is);
//			assertEquals(expectedSnippets, snippets.toString().replaceAll("\\s", ""));
			Parser parser = new Parser();
			parser.init();
			Snippet firstSnip = snippets.getSnippet().get(2);
			parser.parseSnippet(firstSnip);
			assertEquals("(Aguilar 3694;  Costa Rica, INB, MO)", firstSnip.getEspecimen().trim());
			//0-400(-600) metros
			assertEquals("0-400(-600)metros", firstSnip.getElevacion().trim());
			assertEquals(3, firstSnip.getFloracion().size());
		}
		catch(Exception e){
			e.printStackTrace();
			fail("Some exception");
		}
	}
	
	@Test
	public void testbinding3(){
		try{
			System.out.println("#### Salida3.xml ####");
			InputStream is = BindingTest.class.getClassLoader().getResourceAsStream("salida3.xml");
			Snippets snippets = JdomLoader.parseDocument(is);
//			assertEquals(expectedSnippets, snippets.toString().replaceAll("\\s", ""));
			Parser parser = new Parser();
			parser.init();
			Snippet firstSnip = snippets.getSnippet().get(0);
			Map<Distribucion,String>  snippetParseado = parser.parseSnippet(firstSnip);
			assertEquals("(Opler 1848, MO)", snippetParseado.get(Distribucion.ESPECIMEN));
			//0-400(-600) metros
			assertEquals("200-500<metros", snippetParseado.get(Distribucion.ELEVACION));
			assertEquals("julio-set.",snippetParseado.get(Distribucion.FLORACION));
		}
		catch(Exception e){
			e.printStackTrace();
			fail("Some exception");
		}
	}
	
//	@Test
//	public void testbinding4(){
//		try{
//			System.out.println("#### Salida4.xml ####");
//			InputStream is = BindingTest.class.getClassLoader().getResourceAsStream("salida3.xml");
//			Snippets snippets = JdomLoader.parseDocument(is);
////			assertEquals(expectedSnippets, snippets.toString().replaceAll("\\s", ""));
//			Parser parser = new Parser();
//			parser.init();
//			Snippet firstSnip = snippets.getSnippet().get(0);
//			Map<Distribucion,String>  snippetParseado = parser.parseSnippet(firstSnip);
//			assertEquals("(Opler 1848, MO)", snippetParseado.get(Distribucion.ESPECIMEN));
//			//0-400(-600) metros
//			assertEquals("200-500<metros", snippetParseado.get(Distribucion.ELEVACION));
//			assertEquals("julio-set.",snippetParseado.get(Distribucion.FLORACION));
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			fail("Some exception");
//		}
//	}
	
//	private String expectedSnippets = "Snippets[[Snippet[Category[name=distribucion],Taxon[family=Haloragaceae,genre=,species=],"
//			+ "Text=9gén.yaproximadamente145especies,Alaska,CanadáyGroenlandia-SChileyVenezuela,Brazil,Paraguay,Uruguay,Argentina,"
//			+ "IslasMalvinas,AntillasMayores,Bahamas,ViejoMundo;1gén.y1especieenCostaRica.],Snippet[Category[name=distribucion],"
//			+ "Taxon[family=Haloragaceae,genre=Myriophyllum,species=],Text=Aproximadamente60especies,Alaska,CanadáyGroenlandia-SChi"
//			+ "leyVenezuela,Brazil,Paraguay,Uruguay,Argentina,IslasMalvinas,AntillasMayores,Eurasia,Australia.]]]";
}
