package cr.ac.tec.parser.solr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import cr.ac.tec.model.Distribucion;
import cr.ac.tec.model.DistribucionGeo;
import cr.ac.tec.model.ZonaHoldridge;
import cr.ac.tec.parser.BotanicalGeoParser;
import cr.ac.tec.parser.aux.GeneradorIds;

@Component("solrParser")
public class GeoParserSolr implements BotanicalGeoParser {

	@Value(value = "${parser-geografico.solr.url:http://localhost:8983/solr/geonames}")
	private String solrUrl;
	
	@Value(value = "${parser-geografico.solr.zonas.url:http://localhost:8983/solr/zonasdevida}")
	private String zonasDeVidaUrl;
	
	private SolrClient solrClient;
	private SolrClient solrZonasDeVidaClient;

	/**
	 * Method that is executed when the class is instantiated by spring
	 * it will create the solr client with the specified url in solrUrl
	 */
	@PostConstruct
	public void init() {
		solrClient = new HttpSolrClient(solrUrl);
		solrZonasDeVidaClient = new HttpSolrClient(zonasDeVidaUrl); 
		
	}
	
	public List<DistribucionGeo> parsearDistribucion(String texto, Distribucion dist) throws Exception {
		String t = texto.trim().replaceAll("\\(", "").replaceAll("\\)", "");
//		t = t.replace("_", " ");
//		t = "\""+t+"\"";

		Distribucion d = dist;
		
		String fq = "+feature_code:(PCL* OR ADM* OR PRSH OR TERR OR ZN OR ZNB) +feature_class:A";
		
		
		if(d.equals(Distribucion.DISTRIBUCION_CR)){
			fq = "+(alternate_country_code:CR OR country_code:CR) -feature_code:HTL -feature_code:EST";
			
		}
		
			
		SolrQuery query = new SolrQuery();
		query.setRequestHandler("/select");
		query.set("qf","name^10 asciiname^3 alternatenames^3 name_stem asciiname_stem alternatenames_stem");
		query.set("fq",fq);
		query.set("q", t);
		
		if(d.equals(Distribucion.DISTRIBUCION_MUNDO)){
			query.add("bq", "feature_code:ADM1^6");
			query.add("bq", "feature_code:ADMD^6");
			query.add("bq", "feature_code:PCLI^40");
			query.add("bq", "feature_code:PCLD^30");
			query.add("bq", "feature_code:PCLF^20");
			query.add("bq", "feature_code:PCLH^20");
			query.add("bq", "feature_code:ADM2^4");
			query.add("bq", "feature_code:ADM3^2");
			query.add("bq", "feature_code:ADM4");
			query.set("mm", "75%");
		
		}else{
			query.set("mm", "100%");	
		}
		
		QueryResponse response = solrClient.query(query);
		SolrDocumentList list = response.getResults();
		
		List<DistribucionGeo> modeloList = traducirSolrAModelo(list,texto);
		return validarResultados(modeloList) ? modeloList : generarElementoNuevo(t);
	}
	
	/** Metodo que traduce del modelo de Solr al modelo del parser geografico
	 * @param list
	 * @return una lista de DistribucionGeo con un solo elemento o una lista vacia en caso de no encontrar nada
	 */
	private List<DistribucionGeo> traducirSolrAModelo(SolrDocumentList list, String original){
		List<DistribucionGeo> modelList = new ArrayList<DistribucionGeo>();
		if(Objects.nonNull(list) && !list.isEmpty()){
			SolrDocument solrDoc = list.get(0);
			Map<String,Object> mapaCamposYValores = solrDoc.getFieldValueMap();
			DistribucionGeo objetoModelo = new DistribucionGeo();
			objetoModelo.setCodigo((String)mapaCamposYValores.get("geonameid"));
			objetoModelo.setNombre((String)mapaCamposYValores.get("name"));
			objetoModelo.setTipo((String)mapaCamposYValores.get("feature_code"));
			objetoModelo.setLat((Double)mapaCamposYValores.get("latitude"));
			objetoModelo.setLng((Double)mapaCamposYValores.get("longitude"));
			objetoModelo.setOriginal(original);
			
			modelList.add(objetoModelo);
		}
		return modelList;
	}
	
	/** Metodo que traduce del modelo de Solr al modelo del parser geografico para zonas de vida
	 * @param list
	 * @return una lista de ZonaHoldridge con un solo elemento o una lista vacia en caso de no encontrar nada
	 */
	private List<ZonaHoldridge> traducirSolrAModeloZonasDeVida(SolrDocumentList list, String original){
		List<ZonaHoldridge> modelList = new ArrayList<ZonaHoldridge>();
		if(Objects.nonNull(list) && !list.isEmpty()){
			SolrDocument solrDoc = list.get(0);
			Map<String,Object> mapaCamposYValores = solrDoc.getFieldValueMap();
			ZonaHoldridge objetoModelo = new ZonaHoldridge();
			objetoModelo.setCodigo((String)mapaCamposYValores.get("id"));
			objetoModelo.setNombre((String)mapaCamposYValores.get("nombre_es"));
			objetoModelo.setRegion((String)mapaCamposYValores.get("region"));
			objetoModelo.setOriginal(original);
			modelList.add(objetoModelo);
		}
		return modelList;
	}
	
	private List<DistribucionGeo> generarElementoNuevo(String texto){
		List<DistribucionGeo> modelList = new ArrayList<DistribucionGeo>();
		DistribucionGeo objetoModelo = new DistribucionGeo();
		objetoModelo.setCodigo("DESC");
		objetoModelo.setNombre(texto);
		modelList.add(objetoModelo);
		return modelList;
	}
	private List<ZonaHoldridge> generarElementoNuevoZonasDeVida(String texto){
		List<ZonaHoldridge> modelList = new ArrayList<ZonaHoldridge>();
		ZonaHoldridge objetoModelo = new ZonaHoldridge();
		objetoModelo.setCodigo("DESC");
		objetoModelo.setNombre(texto);
		modelList.add(objetoModelo);
		return modelList;
	}
	
	private boolean validarResultados(List list){
		return !list.isEmpty();
	}

	public List<ZonaHoldridge> parsearZonaDeVida(String text) throws Exception {
		
		
		
			
		SolrQuery query = new SolrQuery();
		query.setRequestHandler("/select");
		query.set("qf","nombre_es^100 nombre_en");
		
		query.set("q", text);
		
			query.add("bq", "region:tropical^5000");
			query.add("bq", "region:subtropical^100");
			query.set("mm", "2<90%");
//			query.add("bq", "region:\"templado calido\"^15");
//			query.add("bq", "region:\"templado frio\"^10");
			query.add("bq", "region:boreal^5");
			
		
		
		
		QueryResponse response = solrZonasDeVidaClient.query(query);
		SolrDocumentList list = response.getResults();
		
		List<ZonaHoldridge> modeloList = traducirSolrAModeloZonasDeVida(list,text);
		return validarResultados(modeloList) ? modeloList : generarElementoNuevoZonasDeVida(text);
	}

}
