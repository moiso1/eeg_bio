package cr.ac.tec.parser.clavin;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.bericotech.clavin.ClavinException;
import com.bericotech.clavin.GeoParser;
import com.bericotech.clavin.GeoParserFactory;
import com.bericotech.clavin.gazetteer.GeoName;
import com.bericotech.clavin.resolver.ResolvedLocation;

import cr.ac.tec.model.Distribucion;
import cr.ac.tec.model.DistribucionGeo;
import cr.ac.tec.model.ZonaHoldridge;
import cr.ac.tec.parser.BotanicalGeoParser;
import javax.annotation.PostConstruct;

@Component("clavinparser")
public class GeoParserClavin implements BotanicalGeoParser {
	
//	@Value("${parser-geografico.clavin.ruta-indice}")
	
	@Value(value = "${parser-geografico.clavin.ruta_indice}")
	private String rutaIndice;
	private GeoParser parser;
	@Value("${parser-geografico.clavin.nivel_confianza}")
	private String umbralConfianzaString;
	private float umbralConfianza = 0.5f;
	@PostConstruct
	public void init() throws ClavinException{
		parser = GeoParserFactory.getDefault(rutaIndice, true);
		try{
			umbralConfianza = Float.parseFloat(umbralConfianzaString);
		}
		catch(NumberFormatException nfe){
			throw new ClavinException(nfe);
		}
	}


	public List<DistribucionGeo> parsearDistribucion(String texto, Distribucion dist) throws Exception {
		List<ResolvedLocation> locacionesDeClavin = parser.parse(texto);
		List<DistribucionGeo> resultado = new ArrayList<DistribucionGeo>();
		for(ResolvedLocation loc : locacionesDeClavin){
			float confianza = loc.getConfidence();
			if(confianza >= umbralConfianza){
				DistribucionGeo geoEntrada = new DistribucionGeo();
				GeoName geoName = loc.getGeoname();
				geoEntrada.setCodigo(String.valueOf(geoName.getGeonameID()));
				geoEntrada.setNombre(geoName.getName());
				geoEntrada.setLat(geoName.getLatitude());
				geoEntrada.setLng(geoName.getLongitude());
				geoEntrada.setTipo(geoName.getFeatureClass().toString());
				resultado.add(geoEntrada);
			}
		}
		return resultado;
	}
	

	public String getRutaIndice() {
		return rutaIndice;
	}
	public void setRutaIndice(String rutaIndice) {
		this.rutaIndice = rutaIndice;
	}


	public void setUmbralConfianza(float umbralConfianza) {
		this.umbralConfianza = umbralConfianza;
	}


	public List<ZonaHoldridge> parsearZonaDeVida(String text) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	

}
