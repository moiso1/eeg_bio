package cr.ac.tec.parser.aux;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.PrimitiveIterator.OfLong;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.LongStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

import cr.ac.tec.model.Distribucion;
import cr.ac.tec.model.Distribucion.*;

@Component("generador")
public class GeneradorIds {

	private AtomicLong idDistribucionCR;
	private AtomicLong idDidstribucionMundo;
	private AtomicLong idZonasDeVida;
	@Value("${parser-geografico.porcentaje.prueba:0.1}")
	private String porcentajeDeDatos;
	
	private float porcentajeDatosVal;
	
	private List<Long> idDistribucionCRParaPrueba;
	private List<Long> idDisbtribucionMundoParaPrueba;
	private List<Long> idZonasDeVidaParaPrueba;
	
	public GeneradorIds(){
		this.idDistribucionCR = new AtomicLong();
		this.idDidstribucionMundo = new AtomicLong();
		this.idZonasDeVida = new AtomicLong();
	}
	
	@PostConstruct
	public void init(){
		try{
			porcentajeDatosVal = Float.valueOf(porcentajeDeDatos);
		}
		catch(NumberFormatException nfe){
			nfe.printStackTrace();
		}
	}
	public long generarIdDistribucionCR(){
		return this.idDistribucionCR.getAndIncrement();
	}
	
	public long generarIdDistribucionMundo(){
		return this.idDidstribucionMundo.getAndIncrement();
	}
	
	public long generarIdZonasDeVida(){
		return this.idZonasDeVida.getAndIncrement();
	}
	
	public Long[] generarListaDePrueba(Distribucion dist){
		switch(dist){
			case DISTRIBUCION_CR:
				double r = Math.floor(this.idDistribucionCR.get() * porcentajeDatosVal);
				long t = (long)r;
				return obtenerListaOrdenadaAleatoriosEnRango(t, 0, this.idDistribucionCR.get());
			case DISTRIBUCION_MUNDO:
				double ra = Math.floor(this.idDidstribucionMundo.get() * porcentajeDatosVal);
				long ta = (long)ra;
				return obtenerListaOrdenadaAleatoriosEnRango(ta, 0, this.idDidstribucionMundo.get());
			case ZONAS_DE_VIDA:
				double rz = Math.floor(this.idZonasDeVida.get() * porcentajeDatosVal);
				long tz = (long)rz;
				return obtenerListaOrdenadaAleatoriosEnRango(tz, 0, this.idZonasDeVida.get());
			default:
				double rd = Math.floor(this.idDistribucionCR.get() * porcentajeDatosVal);
				long td = (long)rd;
				return obtenerListaOrdenadaAleatoriosEnRango(td, 0, this.idDistribucionCR.get());
		}
	}
	
	private Long[] obtenerListaOrdenadaAleatoriosEnRango(long tamano, long init, long fin){
		
		Random genRandom = new Random();
		LongStream stream = genRandom.longs(tamano, init, fin);
		long[] comoArreglo = stream.toArray();
		LinkedHashSet<Long> set = new LinkedHashSet<Long>((int)tamano);
		for(long k: comoArreglo){
			set.add(new Long(k));
		}
	
		while(set.size() < tamano){
			set.add(new Long((long)genRandom.nextInt((int)fin+1)));
		}
		Long[] comoArregloLong = set.toArray(new Long[set.size()]);
		Arrays.sort(comoArregloLong);
		return comoArregloLong;
		
		
	}
}
