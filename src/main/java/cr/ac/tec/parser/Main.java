package cr.ac.tec.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import cr.ac.tec.model.Distribucion;
import cr.ac.tec.model.Snippet;
import cr.ac.tec.model.Snippets;

@Component
public class Main {

	public static void main(String[] args) {
		LocalDateTime tiempoInicio = LocalDateTime.now();
		 ApplicationContext context = new ClassPathXmlApplicationContext("classpath:appcontext.xml");
		System.out.println("Instituto Tecnológico de Costa Rica");
		System.out.println("Escuela de Computación");
		System.out.println("Maestría en Ciencias de la Computación");
		Main p = context.getBean(Main.class);
		
		p.start(args);
		LocalDateTime tiempoFinal = LocalDateTime.now();
		
		long diffInSeconds = Duration.between(tiempoInicio, tiempoFinal).getSeconds();
		System.out.println("Duración: "+diffInSeconds+" s.");
	}
	
	@Autowired
	private GeoService service;
	@Value("${parser-geografico.archivo_salida_default}")
	private String archivoSalida;
	
	@Value("${parser-geografico.prefijo_archivo_salida_pruebas_default}")
	private String prefijoSalidaArchivo;
	private void start(String[] args){
        Options options = buildOptions();

        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(options, args);
            if (cmd.hasOption("h")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("parser-geografico", options);
                System.exit(0);
            } else if (cmd.hasOption("v")) { // versioning
                System.out.println("parser-geografico - v.%s".format(version()));
                System.exit(0);
            } else if (cmd.hasOption("i")) {
                String archivo = cmd.getOptionValue("i");
                if (checkDataDir(archivo)) {
                	 
                   if(cmd.hasOption("o")){
                	   archivoSalida = cmd.getOptionValue("o");
                   }
                   if(cmd.hasOption("p")){
                	   prefijoSalidaArchivo = cmd.getOptionValue("p");
                   }
                   
                    work(archivo,archivoSalida,prefijoSalidaArchivo);
                }
               else {
                    System.out.println("ERROR: Archivo: " + archivo + ". No existe.");
                    HelpFormatter formatter = new HelpFormatter();
                    formatter.printHelp("parser-geografico", options);
                    System.exit(0);
                }
            }
            
            else {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("parser-geografico", options);
                System.exit(0);
            }

        } catch (Exception pe) {
            System.out.println(pe.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("parser-geografico", options);
        }
	}
	
	public void work(String archivoEntrada, String archivoSalida, String prefijoSalidaPruebas){
		try{
			InputStream is = new FileInputStream(archivoEntrada);
			Snippets listOfSnippets = JdomLoader.parseDocument(is);
			Snippets resultados = new Snippets();
			List<Snippet> r = new ArrayList<Snippet>();
			for(Snippet snip : listOfSnippets.getSnippet()){
				String species = snip.getTaxon().getSpecies();
				if(Objects.nonNull(species) && !species.isEmpty()){
					r.add(service.parsearSnippet(snip));
				}
			}
			resultados.setSnippet(r);
			OutputStream os = new FileOutputStream(archivoSalida);
			JdomLoader.printXml(os, resultados);
			System.out.println(String.format("Documento : %s", archivoEntrada));
//			System.out.println(resultados);
			imprimirArchivoDePrueba(prefijoSalidaPruebas);
			
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	private void imprimirArchivoDePrueba(String prefijoDeArchivoPruebas) throws IOException{
		Long[] distCR = service.getGeneradorIds().generarListaDePrueba(Distribucion.DISTRIBUCION_CR);
		Long[] distMundo = service.getGeneradorIds().generarListaDePrueba(Distribucion.DISTRIBUCION_MUNDO);
		Long[] zonasDeVida = service.getGeneradorIds().generarListaDePrueba(Distribucion.ZONAS_DE_VIDA);
		imprimirArchivoPrueba(prefijoDeArchivoPruebas+"_"+Distribucion.DISTRIBUCION_CR.name()+".csv", distCR);
		imprimirArchivoPrueba(prefijoDeArchivoPruebas+"_"+Distribucion.DISTRIBUCION_MUNDO.name()+".csv", distMundo);
		imprimirArchivoPrueba(prefijoDeArchivoPruebas+"_"+Distribucion.ZONAS_DE_VIDA.name()+".csv", zonasDeVida);
	}
	private void imprimirArchivoPrueba(String nombre, Long[] datos) throws IOException{
		
		PrintWriter pw = new PrintWriter(new FileWriter(nombre));
		pw.write("Id, Resultado");
		pw.println();
		for(Long dato : datos){
			pw.write(dato.toString());
			pw.write(",");
			pw.println();
		}
		pw.flush();
		pw.close();
			
	}
	
	public static void printUsage(){
		System.out.println("Instrucciones: ");
		System.out.println("java -jar parser-geografico.jar <nombre del archivo>");
	}
	
	private static Options buildOptions() {
        Options options = new Options();

        Option inputFile = Option.builder("i").hasArg()
                .desc("Archivo xml con los snippets.")
                .build();
        
        Option outputFile = Option.builder("o").hasArg()
                .desc("Archivo xml con los snippets etiquetados.")
                .build();
       
        Option version = Option.builder("v")
                .desc("Version del programa en el formato <version>-<build>.")
                .optionalArg(true).build();
        Option help = Option.builder("h").desc("Imprime este mensaje").optionalArg(true).build();
        
        Option pruebas = Option.builder("p").hasArg().desc("prefijo para el archivo de salida de pruebas").optionalArg(true).build();
        
        options.addOption(version);
        options.addOption(help);
        options.addOption(outputFile);
        options.addOption(inputFile);
        options.addOption(pruebas);
        
        return options;
    }
	private static String version() throws IOException{
		InputStream inputStream = Main.class.getResourceAsStream("/META-INF/MANIFEST.MF");
		Manifest manifest = new Manifest(inputStream);
		 
		Attributes attributes = manifest.getMainAttributes();
		String version = attributes.getValue("Implementation-Version");
		return version;
	}
    private static boolean checkDataDir(String dataDir) {
        File f = new File(dataDir);
        return f.exists();
    }
}
