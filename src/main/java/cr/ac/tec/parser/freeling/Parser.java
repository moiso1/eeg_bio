package cr.ac.tec.parser.freeling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

import cr.ac.tec.model.Distribucion;
import cr.ac.tec.model.Snippet;
import cr.ac.tec.parser.BotanicalGeoParser;
import edu.upc.freeling.*;

@Component
public class Parser {

	private static final String ORACION_PREFIX = "oracion_";
	private static final String FREELINGDIR = "/usr/local";
	private static final String DATA = FREELINGDIR + "/share/freeling/";
	private static final String LANG = "es";
	MacoOptions op;
	LangIdent lgid;
	Tokenizer tk;
	Splitter sp;
	Maco mf;
	HmmTagger tg;
	ChartParser parser;
	DepTxala dep;
	Nec neclass;
	@PostConstruct
	public void init() {
		System.loadLibrary("freeling_javaAPI");

		Util.initLocale("default");
		op = new MacoOptions(LANG);
		op.setActiveModules(false, true, true, true, true, true, true, true,
				true, true);

		op.setDataFiles("", DATA + LANG + "/locucions.dat", DATA + LANG
				+ "/quantities.dat", DATA + LANG + "/afixos.dat", DATA + LANG
				+ "/probabilitats.dat", DATA + LANG + "/dicc.src", DATA + LANG
				+ "/np.dat", DATA + "common/punct.dat");
		
		 // Create analyzers.
	    lgid = new LangIdent(DATA + "/common/lang_ident/ident-few.dat");

	    tk = new Tokenizer( DATA + LANG + "/tokenizer.dat" );
	    sp = new Splitter( DATA + LANG + "/splitter.dat" );
	    mf = new Maco( op );

	    tg = new HmmTagger( DATA + LANG + "/tagger.dat", true, 2 );
	    parser = new ChartParser(
	      DATA + LANG + "/chunker/grammar-chunk.dat" );
	    dep = new DepTxala( DATA + LANG + "/dep/dependences.dat",
	     parser.getStartSymbol() );
	    neclass = new Nec( DATA + LANG + "/nerc/nec/nec-ab-poor1.dat" );

//	    Senses sen = new Senses(DATA + LANG + "/senses.dat" ); // sense dictionary
//	    Ukb dis = new Ukb( DATA + LANG + "/ukb.dat" ); // sense disambiguator
	}

	public Map<Distribucion,String> parseSnippet(Snippet snip) {
		String text = snip.getText();
		
		text = removerPosiblePunto(text);
		int inicioEspecimen = text.lastIndexOf("(");
		String especimen = null;
		if(inicioEspecimen >= 0){
			especimen = text.substring(inicioEspecimen);
			text = text.substring(0, inicioEspecimen);
		}
		if(!text.trim().endsWith("."))
			text = text.concat(".");
		
		 ListWord l = tk.tokenize( text );
		 
	      // Split the tokens into distinct sentences.
	      ListSentence ls = sp.split( l, false );

	      // Perform morphological analysis
	      mf.analyze( ls );

	      // Perform part-of-speech tagging.
	      tg.analyze( ls );

	      // Perform named entity (NE) classificiation.
	      neclass.analyze( ls );

//	      sen.analyze( ls );
//	      dis.analyze( ls );
//	      printResults( ls, "tagged" );

	      // Chunk parser
	      parser.analyze( ls );
//	      printResults( ls, "parsed" );
//	      printTagged(ls);
//	      printTree(ls);
	     
	      Map<String,Sentence> mapOraciones = parseTagged(ls);
	      Map<Distribucion,String> segmentos = parseSentencesList(mapOraciones);
	      segmentos.put(Distribucion.ESPECIMEN, especimen);
	      
//	      printTree(segmentos);
//	      printSegmentos(segmentos);
	      return segmentos;
	     
	      
	   //   System.out.println(oraciones);
	      //snip.setListSentence(ls);
	      // Dependency parser
//	      dep.analyze( ls );
//	      printResults( ls, "dep" );

	}
	

	private static String cortarYDevolverEspecimen(String texto){
		String result = "";
		int inicioEspecimen = texto.lastIndexOf("(");
		result = texto.substring(inicioEspecimen);
		return result;
	}
	
	private static void printTree(Map<Distribucion,List<TreeNode>> segmentos){
		Set<Entry<Distribucion,List<TreeNode>>> setDeEntradasDelMapa = segmentos.entrySet();
		for(Entry<Distribucion,List<TreeNode>> entry : setDeEntradasDelMapa){
			
			System.out.println("======= "+entry.getKey().toString() + " =======");
			StringBuilder builder = new StringBuilder();
			for(TreeNode node : entry.getValue()){
//				printParseTree(0, node);
				builder.append(" ");
				String sub = aplanarArbol(node);
				if(sub.equalsIgnoreCase(".") || sub.equalsIgnoreCase(",") )
					builder.deleteCharAt(builder.length() -1);
				builder.append(sub);
			}
			System.out.println(builder.toString().trim());
		}
		
	}
	
	private static void printSegmentos(Map<Distribucion,String> segmentos){
		for(Entry<Distribucion,String> entry : segmentos.entrySet()){
			System.out.println("======= "+entry.getKey().toString() + " =======");
			System.out.println(entry.getValue());
		}
	}
	
	private static String removerPosiblePunto(String s){
		return s.replaceAll("<pp>.", "").replaceAll("<pp/>", "").replaceAll("<pp>", "");
	}
	private static String aplanarArbol(TreeNode tr){
		StringBuilder builder = new StringBuilder();
		aplanarArbolAux(tr, builder);
		return builder.toString().trim();
	}
	private static void aplanarArbolAux(TreeNode tr, StringBuilder builder){
		long nCh = tr.numChildren();
		Word w;
		
		if(nCh == 0) {
			//entonces es una hoja del arbol
			w = tr.getInfo().getWord();
			builder.append(" ");
			if(w.getTag().equalsIgnoreCase("Fp") || w.getTag().equalsIgnoreCase("Fc") )
				builder.deleteCharAt(builder.length() -1);
			builder.append(w.getForm());
		}
		else{
			//es una rama entonces hay que seguir por las ramas
			for( int i = 0; i < nCh; i++ ) {
		        TreeNode child = tr.nthChildRef( i );
		        if( child != null ) {
		        	aplanarArbolAux(child,builder);
		        }
		        else {
		          System.err.println( "ERROR: Unexpected NULL child." );
		        }
		      }
		}
	}
	
	private static void printParseTree( int depth, TreeNode tr ) {
	    Word w;
	    TreeNode child;
	    long nch;

	    // Indentation
	    for( int i = 0; i < depth; i++ ) {
	      System.out.print( "  " );
	    }

	    nch = tr.numChildren();

	    if( nch == 0 ) {
	      // The node represents a leaf
	      if( tr.getInfo().isHead() ) {
	        System.out.print( "+" );
	      }
	      w = tr.getInfo().getWord();
	      System.out.print(
	        "(" + w.getForm() + " " + w.getLemma() + " " + w.getTag() );
//	      printSenses( w );
	      System.out.println( ")" );
	    }
	    else
	    {
	      // The node probably represents a tree
	      if( tr.getInfo().isHead() ) {
	        System.out.print( "+" );
	      }

	      System.out.println( tr.getInfo().getLabel() + "_[" );

	      for( int i = 0; i < nch; i++ ) {
	        child = tr.nthChildRef( i );

	        if( child != null ) {
	          printParseTree( depth + 1, child );
	        }
	        else {
	          System.err.println( "ERROR: Unexpected NULL child." );
	        }
	      }
	      for( int i = 0; i < depth; i++ ) {
	        System.out.print( "  " );
	      }

	      System.out.println( "]" );
	    }
	  }
	
	
	private static void goOverTree(TreeNode tr, List<Word> listaPalabras) {
	   
	    TreeNode child;
	    long nch;


	    nch = tr.numChildren();

	    if( nch == 0 ) {
	      // The node represents a leaf
	      listaPalabras.add(tr.getInfo().getWord());
	   
	    }
	    else
	    {
	      // The node probably represents a tree
	    
	      for( int i = 0; i < nch; i++ ) {
	        child = tr.nthChildRef( i );

	        if( child != null ) {
	        	goOverTree(child,listaPalabras );
	        }
	        else {
	          System.err.println( "ERROR: Unexpected NULL child." );
	        }
	      }
	    }
	  }
	
	private static void printTagged(ListSentence ls){
	      //System.out.println( "-------- TAGGER results -----------" );

	      // get the analyzed words out of ls.
	      ListSentenceIterator sIt = new ListSentenceIterator(ls);
	      while (sIt.hasNext()) {
	        Sentence s = sIt.next();
	        ListWordIterator wIt = new ListWordIterator(s);
	        while (wIt.hasNext()) {
	          Word w = wIt.next();

	          System.out.print(
	            w.getForm() + " " + w.getLemma() + " " + w.getTag() );
//	          printSenses( w );
	          //System.out.println();
	          System.out.print(" | ");
	        }

	        System.out.print(" ========= ");
	      }
	    }
	
	private static Map<String,Sentence> parseTagged(ListSentence ls){
		Map<String,Sentence> result = new HashMap<String,Sentence>();
		 ListSentenceIterator sIt = new ListSentenceIterator(ls);
	     int count  = 0;
	     while (sIt.hasNext()) {
	        Sentence s = sIt.next();
	        String mapName = ORACION_PREFIX+count;
	        result.put(mapName, s);
	        count++;
	     }
		return result;
		
	}
	
	
	
	private static Map<Distribucion,String> parseSentencesList(Map<String, Sentence> oraciones){
		Map<Distribucion,String> r = new HashMap<Distribucion,String>();
		 
		if(oraciones.containsKey(ORACION_PREFIX+"0")){
			StringBuilder zonasDeVida = new StringBuilder();
			StringBuilder elevacion = new StringBuilder();
			StringBuilder distribucionCR = new StringBuilder();
			Sentence hastaFloracion = oraciones.get(ORACION_PREFIX+"0");
			long numDeSubOraciones = hastaFloracion.size();
			int ind = 0;
			for(int i = ind; i < numDeSubOraciones; i++){
				Word w = hastaFloracion.getWords().get(i);
				if(w.getTag().equalsIgnoreCase("Z")){
					ind = i;
					break;
				}
				zonasDeVida.append(" ");
				if(w.getTag().equalsIgnoreCase("Fp") ||
						w.getTag().equalsIgnoreCase("Fc") ||
						w.getTag().equalsIgnoreCase("Fx") 	
				 ) 
					zonasDeVida.deleteCharAt(zonasDeVida.length() - 1);
				zonasDeVida.append(w.getForm());
			}
			r.put(Distribucion.ZONAS_DE_VIDA, zonasDeVida.toString().trim());
			for(int j = ind; j < numDeSubOraciones; j++){
				Word w = hastaFloracion.getWords().get(j);
				if(w.getTag().equalsIgnoreCase("Fx")){
					ind = j;
					break;
				}
				
				elevacion.append(w.getForm());
			}
			r.put(Distribucion.ELEVACION, elevacion.toString().trim());
			ind++; //brincandose el punto y coma (Fx)
			for(int k = ind; k < numDeSubOraciones ; k++){
				Word w = hastaFloracion.getWords().get(k);
				distribucionCR.append(" ");
				if(w.getTag().equalsIgnoreCase("Fc") || w.getTag().equalsIgnoreCase("Fp")
						|| w.getTag().equalsIgnoreCase("Fx")){
					distribucionCR.deleteCharAt(distribucionCR.length() -1 );
				}
				distribucionCR.append(w.getForm());
			}
			r.put(Distribucion.DISTRIBUCION_CR, distribucionCR.toString().trim());
		}
		if(oraciones.containsKey(ORACION_PREFIX+"1")){
			StringBuilder floracion = new StringBuilder();
			StringBuilder distribucionMundo = new StringBuilder();
			Sentence floracionEnAdelante = oraciones.get(ORACION_PREFIX+"1");
			long numSubOracions = floracionEnAdelante.size();
			int index = 0;
			index++;//brinque el termino flor
			for(int i = index ; i < numSubOracions; i++){
				Word w = floracionEnAdelante.getWords().get(i);
				String tag = w.getTag();
				String form = w.getForm();
				if(tag.equalsIgnoreCase("W")){  
//					floracion.add(floracionEnAdelante.nthChildRef(i));
					floracion.append(" ");
					floracion.append(form);
				}
				else if(tag.equalsIgnoreCase("Fx") || tag.equalsIgnoreCase("Fp") || tag.equalsIgnoreCase("Fc") || tag.equalsIgnoreCase("Fg")){
					floracion.append(form);
				}
				else if(esUnRangoDeMes(form)){
					floracion.append(" ");
					floracion.append(form);
				}	
				else{
					index = i;
					break;					
				}
				index = i;
			}
			
			r.put(Distribucion.FLORACION, floracion.toString().trim());
			for(int i = index ; i < numSubOracions; i++){
//				distribucionMundo.add(floracionEnAdelante.nthChildRef(i));
				distribucionMundo.append(" ");
				Word w = floracionEnAdelante.getWords().get(i);
				if(w.getTag().equalsIgnoreCase("Fc") || w.getTag().equalsIgnoreCase("Fp")
						|| w.getTag().equalsIgnoreCase("Fx")){
					distribucionMundo.deleteCharAt(distribucionMundo.length() -1 );
				}
				distribucionMundo.append(w.getForm());
			}

			r.put(Distribucion.DISTRIBUCION_MUNDO, distribucionMundo.toString().trim());
		}
		if(oraciones.size() > 2){//hay mas distribucion en el mundo?
			Sentence despuesDeFloracion = oraciones.get(ORACION_PREFIX+"2");
			long numSubOracions = despuesDeFloracion.size();
			int index = 0;
			StringBuilder distribucionMundo = new StringBuilder();
			for(int i = index ; i < numSubOracions; i++){
//				distribucionMundo.add(floracionEnAdelante.nthChildRef(i));
				distribucionMundo.append(" ");
				Word w = despuesDeFloracion.getWords().get(i);
				if(w.getTag().equalsIgnoreCase("Fc") || w.getTag().equalsIgnoreCase("Fp")
						|| w.getTag().equalsIgnoreCase("Fx")){
					distribucionMundo.deleteCharAt(distribucionMundo.length() -1 );
				}
				distribucionMundo.append(w.getForm());
			}
			if(r.containsKey(Distribucion.DISTRIBUCION_MUNDO)){
				String dist = r.get(Distribucion.DISTRIBUCION_MUNDO);
				dist = dist.concat(distribucionMundo.toString().trim());
				r.put(Distribucion.DISTRIBUCION_MUNDO, dist);
			}
			else{
				r.put(Distribucion.DISTRIBUCION_MUNDO, distribucionMundo.toString().trim());
			}
		}
		
		return r;
		
	}
	
	private static boolean esUnRangoDeMes(String rango){
		String[] meses = rango.split("-");
		if(meses.length == 2){
			return  esUnMes(meses[0].trim()) && esUnMes(meses[0].trim());
		}
		return false;
	}
	private static boolean esUnMes(String posibleMes){
		String entrada = posibleMes.trim();
		 Pattern p = Pattern.compile(BotanicalGeoParser.REGEX_MESES);
		 Matcher m = p.matcher(entrada);
		 return m.matches();
	}
	private static TreeNode getFirstLeafNodeWithTag(String tag, TreeNode node){
		long nCh = node.numChildren();
		if(nCh > 0){
			for(int i = 0; i < nCh; i++){
				TreeNode refToChild = node.nthChildRef(i);
				if(searchInDepth(tag, refToChild, false))
					return refToChild;
			}
		}
		else{
			if(node.getInfo().getWord().getTag().equalsIgnoreCase(tag))
				return node;
		}
		return null;
	}
	
	
	
	private static boolean searchInDepth(String tag, TreeNode node, boolean initial){
		long nch = node.numChildren();
		TreeNode child;
		boolean result = initial;
		if(nch == 0L){
			if(node.getInfo().getWord().getTag().equalsIgnoreCase(tag))
				result |= true;
		}
		else{
	      for( int i = 0; i < nch; i++ ) {
		        child = node.nthChildRef( i );

		        if( child != null ) {
		        	result |= searchInDepth( tag, child,result );
		        }
		        else {
		          System.err.println( "ERROR: Unexpected NULL child." );
		        }
		      }
		}
	    return result;
	}
	
//	private static void getListOfWords(TreeNode node, List<Word> words){
//		long num = node.numChildren();
//		if(num == 0)
//			words.add(node.getInfo().getWord());
//		else{
//			for(int i=0 ; i < )
//		}
//	}
	
	
	
	
	
}
