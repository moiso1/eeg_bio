package cr.ac.tec.parser;

import java.util.Arrays;
import java.util.List;

import cr.ac.tec.model.Distribucion;
import cr.ac.tec.model.DistribucionGeo;
import cr.ac.tec.model.Snippet;
import cr.ac.tec.model.ZonaHoldridge;

public interface BotanicalGeoParser {
	public List<DistribucionGeo> parsearDistribucion(String texto,Distribucion dist) throws Exception; 
	public List<ZonaHoldridge> parsearZonaDeVida(String text) throws Exception;
	public static String[] meses = { "ene.", "feb.", "mar.", "abr.", "may.", "jun.", "jul.", "ago.", "set.", "oct.",
			"nov.", "dic." };
	public  static String[] mesesFull = { "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto",
			"setiembre", "octubre", "noviembre", "diciembre" };
	public static String REGEX_MODIFICADORES = "(S|N|E|O|SE|SO|NE|NO|cerca|vecindad|vecindades|cuenca|región|region|nativa|alrededores|cultivado\\(\\s*a\\s*\\)|cultivado\\(\\s*a\\s*\\)) (.+)$";
	public static String REGEX_SUBORDINADOS = "^\\s*(.+)\\((\\s*.+\\s*)\\)\\.?\\s*$";
	public static String REGEX_RANGOS = "^(.+)-(.+)$";
	public static String REGEX_MESES = "\\b(?:enero|ene|febrero|feb|marzo|mar|abril|abr|mayo|may|junio|jun|julio|jul|agosto|ago|setiembre|septiembre|set|sept|octubre|oct|noviembre|nov|diciembre|dic?)\\.?";
	public  static List<String> mesesLista = Arrays.asList(meses);
	public static List<String> mesesFullLista = Arrays.asList(mesesFull);
}
