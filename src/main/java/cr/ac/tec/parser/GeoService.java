package cr.ac.tec.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import cr.ac.tec.model.Distribucion;
import cr.ac.tec.model.DistribucionGeo;
import cr.ac.tec.model.RangoMundo;
import cr.ac.tec.model.Snippet;
import cr.ac.tec.model.ZonaHoldridge;
import cr.ac.tec.parser.aux.GeneradorIds;
import cr.ac.tec.parser.freeling.Parser;

@Component

public class GeoService {

	@Autowired
	Parser parserGramatical;
	@Autowired
	@Qualifier("solrParser")
	BotanicalGeoParser geoParser;
	@Value("${parser-geografico.preposiciones}")
	String preposiciones;
	@Autowired
	private GeneradorIds generadorIds;

	public Snippet parsearSnippet(Snippet snippet) throws Exception {
		Snippet res = snippet;
		Map<Distribucion, String> segmentos = parserGramatical.parseSnippet(snippet);
		String distribucionCR = segmentos.get(Distribucion.DISTRIBUCION_CR);
		String distribucionMundo = segmentos.get(Distribucion.DISTRIBUCION_MUNDO);

		String floracion = segmentos.get(Distribucion.FLORACION);
		String zonasDeVida = segmentos.get(Distribucion.ZONAS_DE_VIDA);
		if (Objects.nonNull(distribucionCR) && !distribucionCR.isEmpty()) {
//			res.setDistribucionCR(geoParser.parsearDistribucion(distribucionCR));
//			res.setDistribucionCR(procesarDistribucion(distribucionCR, Distribucion.DISTRIBUCION_CR));
			procesarDistribucion(res,distribucionCR, Distribucion.DISTRIBUCION_CR);
		}
		if (Objects.nonNull(distribucionMundo) && !distribucionMundo.isEmpty()) {
//			res.setDistribucionMundo(procesarDistribucion(distribucionMundo,Distribucion.DISTRIBUCION_MUNDO));
			procesarDistribucion(res,distribucionMundo,Distribucion.DISTRIBUCION_MUNDO);
			
		}
		if (Objects.nonNull(floracion) && !floracion.isEmpty()) {
			res.setFloracion(generarMeses(floracion));
		}
		if (Objects.nonNull(zonasDeVida) && !zonasDeVida.isEmpty()){
			procesarZonaDeVida(res, zonasDeVida);
		}
		res.setElevacion(segmentos.get(Distribucion.ELEVACION));
		res.setEspecimen(segmentos.get(Distribucion.ESPECIMEN));

		return res;
	}

	private  List<String> separarPorComa(String texto) {
		String[] arreglo = texto != null ? texto.split(",") : null;
		if (arreglo != null)
			return Arrays.asList(arreglo);
		return null;

	}
	private List<String> revisarPreposiciones(String texto){
		List<String> preps = separarPorComa(preposiciones);
		List<String> separadosPorComa = separarPorComa(texto);
		List<String> resultado = new ArrayList<String>(2);
		String preposicionEncontrada = "";
		boolean comienzaConPrep = false;
		boolean oracionPrep = false;
		boolean primero = false;
		for(String sepPorComa : separadosPorComa){
			
			for(String prep : preps){
				comienzaConPrep = sepPorComa.toLowerCase().trim().startsWith(prep.toLowerCase().trim());
				if(comienzaConPrep){
					preposicionEncontrada = prep;
					primero = true;
					oracionPrep = true;
					break;
				}
			}
			String[] arr = sepPorComa.split(" y ");
			List<String> separadosPorY = Arrays.asList(arr);
			if(arr.length > 1){ //es de la forma:   cords.central y de talamanca
				boolean p = true;
				for(String sY : separadosPorY){
					
					if(primero){
						resultado.add(sY);
						primero =false;
						p = false;
					}
					else{
						if(oracionPrep ){
							resultado.add(preposicionEncontrada + " "+sY);
							if(!p){
								comienzaConPrep = false;
								oracionPrep = false;
							}
							p = false;
						}
						else{
							resultado.add(sY);
							comienzaConPrep = false;
						}
					}	
				}
			}
			else if(oracionPrep){  ///ejemplo cords. central
				if(primero){
					resultado.add(arr[0]);
					primero =false;
				}
				else{
					resultado.add(preposicionEncontrada + " "+arr[0]);
				}
				
			}
			else{
				resultado.add(arr[0]);
			}
		}
		return resultado;
	}
	
	private void procesarDistribucion(Snippet snip, String distribucion, Distribucion dist) {
		try {
			List<DistribucionGeo> res = new ArrayList<DistribucionGeo>();
			List<RangoMundo> rangos = new ArrayList<RangoMundo>();
			List<String> valores = revisarPreposiciones(distribucion);

			for (String val : valores) {
				if (dist.equals(Distribucion.DISTRIBUCION_MUNDO)) {
					List<DistribucionGeo> posiblesRangos = generarRango(val, Distribucion.DISTRIBUCION_MUNDO);
					if (posiblesRangos.size() > 1) {
						RangoMundo rango = new RangoMundo();
						rango.setDistribucionMundo(posiblesRangos);
						rangos.add(rango);
					} else {
						res.addAll(posiblesRangos);
					}
				} else {
					String merge = val.replaceAll("-", "");
					DistribucionGeo enCr = distribucionCadaTag(merge, dist);
					res.add(enCr);
				}

			}

			snip.setRangoMundo(rangos);
			if (dist.equals(Distribucion.DISTRIBUCION_MUNDO))
				snip.setDistribucionMundo(res);
			else
				snip.setDistribucionCR(res);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void procesarZonaDeVida(Snippet snip, String zonasDeVida) {
		List<ZonaHoldridge> res = new ArrayList<ZonaHoldridge>();
		List<String> valores = revisarPreposiciones(zonasDeVida);
		try {
			for (String val : valores) {
				ZonaHoldridge z = zonaDeVidaCadaTag(val);
				if (z != null)
					res.add(z);
			}
			snip.setZonasHoldrigde(res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private ZonaHoldridge zonaDeVidaCadaTag(String posibleZona) throws Exception{
		List<ZonaHoldridge> posiblesZonas = geoParser.parsearZonaDeVida(posibleZona);
		if(posiblesZonas.isEmpty())return null;
		ZonaHoldridge r = posiblesZonas.get(0);
		r.setId(generadorIds.generarIdZonasDeVida());
		return r;
	}
	/**Metodo que rompe el string de entrada, si tiene un termino en parentesis. Pone primero el que no viene en parentesis y luego 
	 * el que viene en parentesis y este seria el subordinado , si es valido, dado que podria ser un termino de palabras que esten 
	 * en el diccionario de stopwords eg. de, el , la, esto, aquello, etc. 
	 * @param posibleTag
	 * @return
	 */
	private List<String> terminoPrimarioySubordinados(String posibleTag){
		List<String> terminosSubordinados = new ArrayList<String>(2);
		Pattern p = Pattern.compile(BotanicalGeoParser.REGEX_SUBORDINADOS);
		Matcher m = p.matcher(posibleTag);
		if(m.find()){
			int k = m.groupCount();
			if(k > 1){
				terminosSubordinados.add(m.group(1));
				String m2 = m.group(2);
				if(m2.trim().length()>2){
					String[] arr = m2.split(",|;");
					for(int i = 0; i < arr.length; i++)
						terminosSubordinados.add(arr[i]);
				}
			}
		}
		else{
			terminosSubordinados.add(posibleTag);
		}
		
		
		
		return terminosSubordinados;
	}
	
	
	private DistribucionGeo distribucionCadaTag(String posibleTag, Distribucion dist) throws Exception{
		String posibleTagNorm = posibleTag.replaceAll("_", " ");
		List<String> terminosSubs = terminoPrimarioySubordinados(posibleTagNorm);
		DistribucionGeo r = null;
		if(!terminosSubs.isEmpty()){
			List<String> padre = obtieneModificador(terminosSubs.get(0));
			if(!padre.isEmpty()){
				List<DistribucionGeo> p = geoParser.parsearDistribucion(padre.size() == 2 ? padre.get(1) : padre.get(0) , dist);
				if(p.isEmpty()){
					return null;
				}
				else{
					r = p.get(0);
					r.setId(dist.equals(Distribucion.DISTRIBUCION_CR) ? generadorIds.generarIdDistribucionCR() : generadorIds.generarIdDistribucionMundo());
					r.setModificador(padre.size() == 2 ? padre.get(0): null);
				}
				if(terminosSubs.size() > 1){
					List<DistribucionGeo> subDistribuciones = new ArrayList<DistribucionGeo>();
					for(String sub : terminosSubs.subList(1, terminosSubs.size())){
						List<String> hijo = obtieneModificador(sub);	
						if(!hijo.isEmpty()){
							List<DistribucionGeo> h = geoParser.parsearDistribucion(hijo.size() == 2 ? hijo.get(1) : hijo.get(0) , dist);
							if(!h.isEmpty()){
								h.get(0).setModificador(hijo.size() == 2 ? hijo.get(0) : null);
								subDistribuciones.addAll(h);
							}
						}
					}
					for(DistribucionGeo g : subDistribuciones){
						g.setId(dist.equals(Distribucion.DISTRIBUCION_CR) ? generadorIds.generarIdDistribucionCR() : generadorIds.generarIdDistribucionMundo());
					}
					r.setSubDistribuciones(subDistribuciones);
				}
			}
		}
		
		return r;
	}

	private List<String> obtieneModificador(String termino){
		Pattern p = Pattern.compile(BotanicalGeoParser.REGEX_MODIFICADORES);
		Matcher m = p.matcher(termino);
		List<String> r = new ArrayList<String>();
		if(m.find())
		{
			if(m.groupCount() >1){
				r.add(m.group(1));
				r.add(m.group(2));
			}
		}
		else{
			r.add(termino);
		}
		return r;
	}
	private List<DistribucionGeo> generarRango(String posibleTag, Distribucion dist) throws Exception{
		List<DistribucionGeo> r = new ArrayList<DistribucionGeo>();
		Pattern p = Pattern.compile(BotanicalGeoParser.REGEX_RANGOS);
		Matcher m = p.matcher(posibleTag);
		List<String> rango = new ArrayList<String>();
		if(m.find()){
			if(m.groupCount() > 1){
				rango.add(m.group(1));
				rango.add(m.group(2));
			}
		}
		else{
			rango.add(posibleTag);
		}
		for(String t : rango){
			DistribucionGeo distT = distribucionCadaTag(t, dist);
			if(Objects.nonNull(distT))
				r.add(distT);
		}
		return r;
	}
	private static List<String> resolverRangosMeses(String posibleMes) {
		String[] partidoConGuion = posibleMes.split("-");
		if (partidoConGuion.length > 1) {
			String inicio = partidoConGuion[0].trim();			
			String fin = partidoConGuion[1].trim();
			int inicioIndex = BotanicalGeoParser.mesesLista.lastIndexOf(inicio);
			if (inicioIndex < 0)
				inicioIndex = BotanicalGeoParser.mesesFullLista.lastIndexOf(inicio);

			int finIndex = BotanicalGeoParser.mesesLista.lastIndexOf(fin);
			if (finIndex < 0)
				finIndex = BotanicalGeoParser.mesesFullLista.lastIndexOf(fin);

			if (inicioIndex >= 0 && finIndex >= 0) {
				List<String> mesesEnRango = new ArrayList<String>();
				for (int i = inicioIndex; i <= finIndex; i++) {
					mesesEnRango.add(BotanicalGeoParser.mesesFull[i]);
				}
				return mesesEnRango;
			}
			return null;
		} else {
			return null;
		}
	}

	/**
	 * genera los meses apartir de una lista, ya sea de meses individuales o
	 * rangos
	 * 
	 * @param mesesDeFloracion
	 *            en un formato enero, marzo , jun.-oct., diciembre
	 * @return una lista con los rangos normalizados a toda la lista
	 */
	private  List<String> generarMeses(String mesesDeFloracion) {
		List<String> res = new ArrayList<String>();
		List<String> mesesOriginales = separarPorComa(mesesDeFloracion);
		for (String posibleMes : mesesOriginales) {
			List<String> posibleRango = resolverRangosMeses(posibleMes.trim());
			if (Objects.nonNull(posibleRango))
				res.addAll(posibleRango);
			else {
				int indexAbrev = BotanicalGeoParser.mesesLista.lastIndexOf(posibleMes.trim());
				if (indexAbrev > -1)
					res.add(BotanicalGeoParser.mesesFull[indexAbrev]);
				else
					res.add(posibleMes.trim());
			}
		}
		return res;
	}

	public GeneradorIds getGeneradorIds() {
		return generadorIds;
	}
	
	
}
