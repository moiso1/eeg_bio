package cr.ac.tec.parser;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


import cr.ac.tec.model.Snippets;

public class JdomLoader {
	
    public static Snippets parseDocument(InputStream input) throws IOException, JAXBException{
        JAXBContext jaxbContext = JAXBContext.newInstance(Snippets.class);
     	Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
     	Snippets snippetsObj = (Snippets)jaxbUnmarshaller.unmarshal(input);
     	return snippetsObj;
 
    }
    public static void printXml(OutputStream output, Snippets snips) throws IOException, JAXBException{
    	JAXBContext jaxbContext = JAXBContext.newInstance(Snippets.class);
    	Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    	jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    	jaxbMarshaller.marshal(snips, output);
    }
}
