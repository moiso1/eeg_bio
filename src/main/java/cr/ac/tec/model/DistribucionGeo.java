package cr.ac.tec.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class DistribucionGeo {
	private long id;
	private String nombre;
	private String codigo;
	private double lng;
	private double lat;
	private String tipo;
	private String modificador;
	private String original;
	private List<DistribucionGeo> subDistribuciones;
	public String getNombre() {
		return nombre;
	}
	@XmlElement(name = "nombre")
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodigo() {
		return codigo;
	}
	@XmlElement(name = "codigo")
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public double getLng() {
		return lng;
	}
	@XmlElement(name = "longitud")
	public void setLng(double lng) {
		this.lng = lng;
	}
	public double getLat() {
		return lat;
	}
	@XmlElement(name = "latitud")
	public void setLat(double lat) {
		this.lat = lat;
	}
	public String getTipo() {
		return tipo;
	}
	@XmlElement(name = "tipo")
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getModificador() {
		return modificador;
	}
	@XmlElement(name = "modificador")
	public void setModificador(String modificador) {
		this.modificador = modificador;
	}
	public List<DistribucionGeo> getSubDistribuciones() {
		return subDistribuciones;
	}
	@XmlElement(name = "sub-distribucion")
	public void setSubDistribuciones(List<DistribucionGeo> subDistribuciones) {
		this.subDistribuciones = subDistribuciones;
	}
	public long getId() {
		return id;
	}
	@XmlElement(name = "id")
	public void setId(long id) {
		this.id = id;
	}
	public String getOriginal() {
		return original;
	}
	@XmlElement(name = "original")
	public void setOriginal(String original) {
		this.original = original;
	}
	
	
}
