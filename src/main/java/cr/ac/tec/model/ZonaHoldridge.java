package cr.ac.tec.model;

import javax.xml.bind.annotation.XmlElement;

public class ZonaHoldridge {
	
	private long id;
	private String nombre;
	private String codigo;
	private String region;
	private String original;
	public String getNombre() {
		return nombre;
	}
	@XmlElement(name = "nombre")
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodigo() {
		return codigo;
	}
	
	@XmlElement(name = "codigo")
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getRegion() {
		return region;
	}
	@XmlElement(name = "region")
	public void setRegion(String region) {
		this.region = region;
	}
	public long getId() {
		return id;
	}
	@XmlElement(name = "id")
	public void setId(long id) {
		this.id = id;
	}
	public String getOriginal() {
		return original;
	}
	@XmlElement(name = "original")
	public void setOriginal(String original) {
		this.original = original;
	}
	
	
}
