package cr.ac.tec.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "snippets")
public class Snippets {
	private List<Snippet> snippet;

	public List<Snippet> getSnippet() {
		return snippet;
	}

	@XmlElement(name = "snippet")
	public void setSnippet(List<Snippet> snippet) {
		this.snippet = snippet;
	}

	@Override
	public String toString() {
		return "Snippets ["+snippet+"]";
	}

	
}
