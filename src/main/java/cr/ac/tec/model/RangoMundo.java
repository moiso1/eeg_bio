package cr.ac.tec.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class RangoMundo {
	private List<DistribucionGeo> distribucionMundo;

	public List<DistribucionGeo> getDistribucionMundo() {
		return distribucionMundo;
	}
	@XmlElement(name="ditribucion-mundo")
	public void setDistribucionMundo(List<DistribucionGeo> distribucionMundo) {
		this.distribucionMundo = distribucionMundo;
	}
	
}
