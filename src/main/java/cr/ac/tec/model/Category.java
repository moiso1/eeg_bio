package cr.ac.tec.model;

import javax.xml.bind.annotation.XmlElement;
public class Category {
	private String name;

	public String getName() {
		return name;
	}

	@XmlElement(name = "name")
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Category [name="+this.name+"]";
	}
	

	
}
