package cr.ac.tec.model;

import javax.xml.bind.annotation.XmlElement;

public class Taxon {
	private String family, genre, species;

	public String getFamily() {
		return family;
	}
	@XmlElement(name = "family")
	public void setFamily(String family) {
		this.family = family;
	}

	public String getGenre() {
		return genre;
	}

	@XmlElement(name = "genre")
	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getSpecies() {
		return species;
	}

	@XmlElement(name = "species")
	public void setSpecies(String species) {
		this.species = species;
	}
	@Override
	public String toString() {
		return "Taxon[family="+family+", genre=" + genre+ ", species="+species+"]"; 
	}
	

}
