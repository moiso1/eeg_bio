package cr.ac.tec.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import edu.upc.freeling.ListSentence;

public class Snippet {
	private Category category;
	private Taxon taxon;
	private String text;
	private String elevacion;
//	private ListSentence listSentence;
	private List<ZonaHoldridge> zonasHoldrigde;
	private List<DistribucionGeo> distribucionCR;
	private List<String> floracion;
	private List<DistribucionGeo> distribucionMundo;
	private List<RangoMundo> rangoMundo;
	private String especimen;
	
	public Category getCategory() {
		return category;
	}
	@XmlElement(name = "category")
	public void setCategory(Category category) {
		this.category = category;
	}
	public Taxon getTaxon() {
		return taxon;
	}
	@XmlElement(name = "taxon")
	public void setTaxon(Taxon taxon) {
		this.taxon = taxon;
	}
	public String getText() {
		return text;
	}
	@XmlElement(name = "text")
	public void setText(String text) {
		this.text = text;
	}
	@Override
	public String toString() {
		
		return "Snippet ["+category.toString()+", "+taxon.toString() + ", Text = "+ text.toString()+"]";
	}

	public String getElevacion() {
		return elevacion;
	}
	@XmlElement(name="elevacion")
	public void setElevacion(String elevacion) {
		this.elevacion = elevacion;
	}
	public List<ZonaHoldridge> getZonasHoldrigde() {
		return zonasHoldrigde;
	}
	@XmlElement(name="zonas-holdridge")
	public void setZonasHoldrigde(List<ZonaHoldridge> zonasHoldrigde) {
		this.zonasHoldrigde = zonasHoldrigde;
	}
	public List<DistribucionGeo> getDistribucionCR() {
		return distribucionCR;
	}
	@XmlElement(name="distribucion-cr")
	public void setDistribucionCR(List<DistribucionGeo> distribucionCR) {
		this.distribucionCR = distribucionCR;
	}
	public List<String> getFloracion() {
		return floracion;
	}
	@XmlElement(name="floracion")
	public void setFloracion(List<String> floracion) {
		this.floracion = floracion;
	}
	
	public List<DistribucionGeo> getDistribucionMundo() {
		return distribucionMundo;
	}
	
	@XmlElement(name="ditribucion-mundo")
	public void setDistribucionMundo(List<DistribucionGeo> distribucionMundo) {
		this.distribucionMundo = distribucionMundo;
	}
	public String getEspecimen() {
		return especimen;
	}
	@XmlElement(name="especimen")
	public void setEspecimen(String especimen) {
		this.especimen = especimen;
	}
	public List<RangoMundo> getRangoMundo() {
		return rangoMundo;
	}
	@XmlElement(name="rango-mundo")
	public void setRangoMundo(List<RangoMundo> rangoMundo) {
		this.rangoMundo = rangoMundo;
	}

	
	
}
