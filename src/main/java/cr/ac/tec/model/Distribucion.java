package cr.ac.tec.model;

public enum Distribucion {
	ZONAS_DE_VIDA, ELEVACION, DISTRIBUCION_CR, FLORACION, DISTRIBUCION_MUNDO, ESPECIMEN
}
